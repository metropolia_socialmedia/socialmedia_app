## Prerequisites

 - Android Studio (or alternative)
 - API of the project (accesible from outside source)

## Building
After you opened the app in android studio, wait until the gradle build is finished.
After this you should connect to a firebase project, And add the ML kit to the project:
**tools -> firebase -> ML Kit -> Use ML kit to recognize faces in images and videos **
when you did this the app will add the dependencies needed.

After this you should change the IP in the IUserRepo.kt in the repositories folder.

When this is done you should be able to use the app.

