package krbe.watchdogs.pocketface.UI.AddFriend


import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.ar.sceneform.ArSceneView
import com.google.ar.sceneform.ux.ArFragment
import krbe.watchdogs.pocketface.Helpers.FaceDetection
import org.opencv.android.OpenCVLoader
import java.lang.Exception
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions
import kotlinx.android.synthetic.main.dialog_loading_bar.*
import kotlinx.android.synthetic.main.fragment_add_friend.view.*
import krbe.watchdogs.pocketface.Helpers.ImageHelper
import krbe.watchdogs.pocketface.Helpers.UserHelper
import krbe.watchdogs.pocketface.R
import krbe.watchdogs.pocketface.Repositories.AIRepo
import krbe.watchdogs.pocketface.Repositories.FriendRepo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * A simple [Fragment] subclass.
 */
class AddFriendFragment : Fragment() {
    lateinit var arSceneView: ArSceneView
    lateinit var mErrorDialog: AlertDialog
    lateinit var loadingDialog: AlertDialog
    val scanImage = View.OnClickListener {
        val frame = arSceneView.arFrame

        loadingDialog.show()
        loadingDialog.txt_reason.text = getString(R.string.loading_processing)
        if (frame != null) {
            try {
                val image = frame!!.acquireCameraImage()

                val detection = FaceDetection(image)
                try {
                    var bitmap = detection.getBitmapobject()
                    val image = FirebaseVisionImage.fromBitmap(bitmap)
                    val options = FirebaseVisionFaceDetectorOptions.Builder()
                        .build()
                    val detector = FirebaseVision.getInstance().getVisionFaceDetector(options)
                    detector.detectInImage(image).addOnSuccessListener {
                        // DEBUG
                        /*bitmap = detection.drawRect(bitmap, it)
                        imgDebug.setImageBitmap(bitmap)*/
                        if (!it.isEmpty()) {
                            bitmap = detection.Crop(bitmap, it)
                            //imgDebug.setImageBitmap(bitmap)
                            val image = ImageHelper.encodeImage(bitmap)
                            GetFriend(image)
                        } else {
                            loadingDialog.dismiss()
                            createErrorDialog()
                            mErrorDialog.setTitle(getString(R.string.noface_processing))
                            mErrorDialog.setMessage(getString(R.string.noface_content_processing))
                            mErrorDialog.show()

                        }

                    }.addOnFailureListener {
                        loadingDialog.dismiss()

                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    loadingDialog.dismiss()

                }
                image.close()
            } catch (e: Exception) {
                e.printStackTrace()
                loadingDialog.dismiss()

            }
        }
    }
    val friendCallBack = object : Callback<FriendRepo.Model.Friend> {
        override fun onFailure(call: Call<FriendRepo.Model.Friend>, t: Throwable) {
        }

        override fun onResponse(
            call: Call<FriendRepo.Model.Friend>,
            response: Response<FriendRepo.Model.Friend>
        ) {
            if (response.isSuccessful) {
                try {
                    val friend: FriendRepo.Model.Friend = response.body()!!
                    val call =
                        FriendRepo.service.addFriend(UserHelper.ActiveUser?.token!!, friend.user!!)
                    val callback = object : Callback<FriendRepo.Model.Friend> {
                        override fun onFailure(call: Call<FriendRepo.Model.Friend>, t: Throwable) {
                            loadingDialog.dismiss()
                            createErrorDialog()
                            mErrorDialog.setMessage(getString(R.string.failed_request_title))
                        }

                        override fun onResponse(
                            call: Call<FriendRepo.Model.Friend>,
                            response: Response<FriendRepo.Model.Friend>
                        ) {
                            loadingDialog.dismiss()
                            createErrorDialog()
                            if (response.isSuccessful) {
                                val friend: FriendRepo.Model.Friend = response.body()!!
                                mErrorDialog.setTitle(getString(R.string.friend_added_title))
                                mErrorDialog.setMessage(
                                    getString(
                                        R.string.friend_added_content,
                                        friend.user?.username!!
                                    )
                                )
                            } else {
                                mErrorDialog.setTitle(getString(R.string.failed_request_title))
                                mErrorDialog.setMessage(
                                    getString(
                                        R.string.failed_request_content,
                                        response.code().toString()
                                    )
                                )
                            }
                            mErrorDialog.show()
                        }
                    }
                    call.enqueue(callback)
                } catch (e: Exception) {
                    loadingDialog.dismiss()
                    createErrorDialog()
                    mErrorDialog.setMessage(getString(R.string.failed_request_title))
                    e.printStackTrace()
                }
            } else {
                loadingDialog.dismiss()
                createErrorDialog()
                if (response.code() == 404) {
                    mErrorDialog.setTitle(getString(R.string.not_recognized_processing_title))
                    mErrorDialog.setMessage(getString(R.string.not_recognized_processing_content))
                } else {
                    mErrorDialog.setTitle(getString(R.string.failed_request_title))
                    mErrorDialog.setMessage(
                        (getString(
                            R.string.failed_request_content,
                            response.code().toString()
                        ))
                    )
                }
                mErrorDialog.show()
            }
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_add_friend, container, false)
        val fragment = childFragmentManager.findFragmentById(R.id.sceneform_fragment) as ArFragment
        arSceneView = fragment.arSceneView
        OpenCVLoader.initDebug()
        root.btnScanImage.setOnClickListener(scanImage)
        val loadingdialog =
            LayoutInflater.from(context!!).inflate(R.layout.dialog_loading_bar, null)
        loadingDialog = AlertDialog.Builder(context!!)
            .setView(loadingdialog)
            .setCancelable(false)
            .create()
        return root
    }

    private fun createErrorDialog() {
        val builder = AlertDialog.Builder(context)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton(getString(R.string.ok)) { dialog, which ->
            dialog.dismiss()
        }

        // Finally, make the alert dialog using builder
        mErrorDialog = builder.create()
    }

    private fun GetFriend(image: String) {
        val call = AIRepo.service.recognize(
            UserHelper.ActiveUser?.token!!,
            AIRepo.Model.Image(null, image, null)
        )
        call.enqueue(friendCallBack)
    }
}
