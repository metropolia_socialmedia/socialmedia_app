package krbe.watchdogs.pocketface.UI.Profile


import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.dialog_add_image.btnCancel
import kotlinx.android.synthetic.main.dialog_add_public_data.*
import kotlinx.android.synthetic.main.dialog_friends.*
import kotlinx.android.synthetic.main.dialog_image_preview.*
import kotlinx.android.synthetic.main.dialog_images.*
import kotlinx.android.synthetic.main.dialog_images.btnClose
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import kotlinx.android.synthetic.main.list_item_image.view.*
import kotlinx.android.synthetic.main.list_item_post.view.*
import krbe.watchdogs.pocketface.Adapters.*
import krbe.watchdogs.pocketface.Helpers.UserHelper
import krbe.watchdogs.pocketface.R
import krbe.watchdogs.pocketface.Repositories.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {

    val profileData: MutableList<List<String>> = mutableListOf()
    val friendsData: MutableList<FriendRepo.Model.Friend> = mutableListOf()
    val imageData: MutableList<AIRepo.Model.Image> = mutableListOf()
    val postData = mutableListOf<PostRepo.Model.Post>()
    val publicCallback = object : Callback<ProfileRepo.Model.Profile> {
        override fun onFailure(call: Call<ProfileRepo.Model.Profile>, t: Throwable) {

        }

        override fun onResponse(
            call: Call<ProfileRepo.Model.Profile>,
            response: Response<ProfileRepo.Model.Profile>
        ) {
            if (response.isSuccessful) {
                val profile: ProfileRepo.Model.Profile = response.body()!!
                if (!profile.values!!.isEmpty()) {
                    profileData.clear()
                    profileData.addAll(profile.values)
                    rcvData.visibility = View.VISIBLE
                    txtInfoError.text = ""

                } else {
                    txtInfoError.text = getString(R.string.no_data)
                    rcvData.visibility = View.GONE
                }
            } else {
                txtInfoError.text = getString(R.string.failed_request_title)
                rcvData.visibility = View.GONE
            }
        }

    }
    val onImageClick = View.OnClickListener {
        val inflater = LayoutInflater.from(context).inflate(R.layout.dialog_image_preview, null)
        val alert = AlertDialog.Builder(context).setView(inflater)
            .create()
        alert.show()
        alert.btnCloseImage.setOnClickListener {
            alert.dismiss()
        }
        if (it.imgImage != null) {
            alert.imgSelected.setImageDrawable(it.imgImage.drawable)
        } else if (it.img_content != null) {
            alert.imgSelected.setImageDrawable(it.img_content.drawable)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_profile, container, false)
        root.txtUsername.text = UserHelper.ActiveUser?.username
        initiateAdapters(root)
        initiateDialogs(root)
        getPublicData()
        getFriends()
        getImages()
        getPosts(0)
        return root
    }

    private fun initiateDialogs(root: View) {
        val imgInflater = LayoutInflater.from(context).inflate(R.layout.dialog_images, null)
        val friendInflater = LayoutInflater.from(context).inflate(R.layout.dialog_friends, null)
        val profileInflater =
            LayoutInflater.from(context).inflate(R.layout.dialog_add_public_data, null)
        val imageDialog: AlertDialog = createDialog(imgInflater)
        val friendDialog = createDialog(friendInflater)
        val profileDialog = createDialog(profileInflater)
        root.btnAdd.setOnClickListener {
            profileDialog.show()
            profileDialog.btnCancel.setOnClickListener {
                profileDialog.dismiss()
            }
            profileDialog.btnPost.setOnClickListener {
                val list = mutableListOf<String>()
                list.add(profileDialog.edtName.text.toString())
                list.add(profileDialog.edtVal.text.toString())
                list.add("true")
                val suplist = mutableListOf<List<String>>()
                suplist.add(list)
                val call = ProfileRepo.service.addToProfile(
                    UserHelper.ActiveUser?.token!!,
                    ProfileRepo.Model.Profile(suplist, null)
                )
                call.enqueue(publicCallback)
                profileDialog.dismiss()
            }
        }
        root.btnSeeMoreImg.setOnClickListener {
            imageDialog.show()
            val recycler = imageDialog.rcvCompleteImageList
            recycler.layoutManager = GridLayoutManager(context, 3)
            recycler.addItemDecoration(GridItemDecoration(10, 3))

            recycler.adapter = ProfileImageAdapter(imageData, context!!, onImageClick)
            imageDialog.btnClose.setOnClickListener {
                imageDialog.dismiss()
            }
        }
        root.btnSeeFriends.setOnClickListener {
            friendDialog.show()
            friendDialog.btnClose.setOnClickListener {
                friendDialog.dismiss()
            }
            val recycler = friendDialog.rcvFriendList
            recycler.layoutManager = LinearLayoutManager(context)
            recycler.adapter = ProfileFriendAdapter(friendsData, context!!, null, null)
        }

    }

    private fun createDialog(inflater: View?): AlertDialog {
        return AlertDialog.Builder(context).setView(inflater)
            .create()
    }

    private fun initiateAdapters(root: View) {
        try {
            root.rcvPosts.layoutManager = LinearLayoutManager(context)
            root.rcvPosts.adapter = PostAdapter(postData, context!!, onImageClick)
            root.rcvImages.layoutManager = GridLayoutManager(context, 4)
            root.rcvImages.addItemDecoration(GridItemDecoration(16, 4))
            root.rcvImages.adapter = ProfileImageAdapter(imageData, context!!, onImageClick)
            root.rcvData.layoutManager = LinearLayoutManager(context)
            root.rcvData.adapter = ProfileInfoAdapter(profileData, context!!, null)
            root.rcvFriends.layoutManager = LinearLayoutManager(context)
            root.rcvFriends.adapter =
                ProfileFriendAdapter(friendsData, context!!, null, root.txtFriendsTitle)

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getPublicData() {
        val call = ProfileRepo.service.getUserProfile(
            UserHelper.ActiveUser?.token!!,
            UserHelper.ActiveUser?.userid!!
        )

        call.enqueue(publicCallback)
    }

    private fun getFriends() {
        val call = FriendRepo.service.getFriends(UserHelper.ActiveUser?.token!!)

        val callback = object : Callback<List<FriendRepo.Model.Friend>> {
            override fun onFailure(call: Call<List<FriendRepo.Model.Friend>>, t: Throwable) {
                txtFriendsTitle.text = getString(R.string.friend_profile_title, 0)
                txtFriendsError.text = getString(R.string.failed_request_title)
                rcvFriends.visibility = View.GONE
            }

            override fun onResponse(
                call: Call<List<FriendRepo.Model.Friend>>,
                response: Response<List<FriendRepo.Model.Friend>>
            ) {
                if (response.isSuccessful) {
                    val friends = response.body()!!
                    friendsData.addAll(friends)
                    txtFriendsTitle.text =
                        getString(R.string.friend_profile_title, friendsData.count())
                    rcvFriends.visibility = View.VISIBLE
                } else {
                    txtFriendsTitle.text = getString(R.string.friend_profile_title, 0)
                    txtFriendsError.text = getString(R.string.not_fount_friends)
                    rcvFriends.visibility = View.GONE
                }
            }

        }
        call.enqueue(callback)
    }

    private fun getImages() {
        val call = ImageRepo.service.getImages(UserHelper.ActiveUser?.token!!)
        val callback = object : Callback<List<AIRepo.Model.Image>> {
            override fun onFailure(call: Call<List<AIRepo.Model.Image>>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<List<AIRepo.Model.Image>>,
                response: Response<List<AIRepo.Model.Image>>
            ) {
                if (response.isSuccessful) {
                    imageData.addAll(response.body()!!)
                    txtPhotos.text = getString(R.string.image_profile_title, imageData.count())
                }
            }

        }
        call.enqueue(callback)
    }

    private fun getPosts(page: Int = 0) {
        val call = PostRepo.service.getUserPosts(UserHelper.ActiveUser?.token!!, page)
        val callback = object : Callback<List<PostRepo.Model.Post>> {
            override fun onFailure(call: Call<List<PostRepo.Model.Post>>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<List<PostRepo.Model.Post>>,
                response: Response<List<PostRepo.Model.Post>>
            ) {
                if (response.isSuccessful) {
                    postData.addAll(response.body()!!)
                }
            }
        }
        call.enqueue(callback)
    }
}
