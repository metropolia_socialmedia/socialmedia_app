package krbe.watchdogs.pocketface.UI.Feed


import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.dialog_create_post.*
import kotlinx.android.synthetic.main.dialog_see_post.*
import kotlinx.android.synthetic.main.fragment_feed.*
import kotlinx.android.synthetic.main.fragment_feed.view.*
import kotlinx.android.synthetic.main.list_item_post.*
import kotlinx.android.synthetic.main.list_item_post.view.*
import kotlinx.android.synthetic.main.list_item_post.view.txt_postuser
import krbe.watchdogs.pocketface.Adapters.PostAdapter
import krbe.watchdogs.pocketface.Helpers.ImageHelper
import krbe.watchdogs.pocketface.Helpers.InputChecker
import krbe.watchdogs.pocketface.Helpers.PermissionsHandler
import krbe.watchdogs.pocketface.Helpers.UserHelper
import krbe.watchdogs.pocketface.LoginActivity
import krbe.watchdogs.pocketface.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import krbe.watchdogs.pocketface.Repositories.PostRepo
import krbe.watchdogs.pocketface.Repositories.UserApi
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class FeedFragment : Fragment(), View.OnClickListener {
    lateinit var mCurrentPhotoPath: String
    lateinit var mDialog: AlertDialog
    lateinit var refresher: SwipeRefreshLayout
    var askedpage = 0
    var isloading = false
    var reachedEnd = false
    val REQUEST_IMAGE_CAPTURE = 1
    val postlist = mutableListOf<PostRepo.Model.Post>()
    val postCallback = object : Callback<List<PostRepo.Model.Post>> {
        override fun onFailure(call: Call<List<PostRepo.Model.Post>>, t: Throwable) {
            Log.d("WD", t.toString())
        }

        override fun onResponse(
            call: Call<List<PostRepo.Model.Post>>,
            response: Response<List<PostRepo.Model.Post>>
        ) {
            if (response.isSuccessful) {
                val list: List<PostRepo.Model.Post> = response.body()!!
                if (list.isEmpty()) {
                    reachedEnd = true
                } else {
                    postlist.addAll(list)
                    if (rcvPosts != null) {
                        (rcvPosts.adapter as PostAdapter).alterlist(postlist)
                    }
                }
                refresher.isRefreshing = false
                isloading = false
            } else {
                if (response.code() == 401) {
                    logout()
                }
            }
        }

    }
    val tokenCallback = object : Callback<UserApi.Model.User> {
        override fun onFailure(call: Call<UserApi.Model.User>, t: Throwable) {
            logout()
        }

        override fun onResponse(
            call: Call<UserApi.Model.User>,
            response: Response<UserApi.Model.User>
        ) {
            if (response.isSuccessful) {
                UserHelper.ActiveUser = response.body()
                val token = UserHelper.ActiveUser?.token
                if (askedpage == 0) {
                    postlist.clear()
                }
                val call = PostRepo.service.getPosts(token!!, askedpage)
                call.enqueue(postCallback)
            } else {
                logout()
            }
        }
    }
    val onItemClick = View.OnClickListener { view ->
        val dialoglayout = LayoutInflater.from(context).inflate(R.layout.dialog_see_post, null)
        val dialog = AlertDialog.Builder(context)
            .setView(dialoglayout)
            .create()
        dialog.show()
        dialog.txtProfileIntro.text = view.txt_postuser.text
        dialog.txtDateTime.text = view.txt_date.text
        dialog.imgProfilePic.setImageDrawable(view.img_postuser.drawable)
        dialog.imgContent.setImageDrawable(view.img_content.drawable)
        dialog.txtPostContent.text = view.txt_content.text
        dialog.btnClose.setOnClickListener { dialog.dismiss() }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root =
            inflater.inflate(krbe.watchdogs.pocketface.R.layout.fragment_feed, container, false)
        root.findViewById<FloatingActionButton>(R.id.fabAddPost).setOnClickListener(this)
        root.rcvPosts.adapter = PostAdapter(mutableListOf(), context!!, onItemClick)
        root.rcvPosts.layoutManager = LinearLayoutManager(context)
        refresher = root.srlSwipeRec
        refresher.setOnRefreshListener {
            askedpage = 0
            reachedEnd = false
            getPosts()
        }
        root.rcvPosts.setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (!isloading && !reachedEnd) {
                if ((rcvPosts.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition() == postlist.size - 2) {
                    askedpage++
                    getPosts()
                }
            }
        }
        getPosts()
        return root
    }

    override fun onClick(v: View?) {
        when (v) {
            fabAddPost -> {
                val mDialogView =
                    LayoutInflater.from(context).inflate(R.layout.dialog_create_post, null)
                val mBuilder = AlertDialog.Builder(context)
                    .setView(mDialogView)
                    .setTitle(getString(R.string.create_post_title))
                mDialog = mBuilder.show()
                mDialog.btnCancel.setOnClickListener {
                    mDialog.dismiss()
                }
                mDialog.btnPost.setOnClickListener {
                    val mDialogView =
                        LayoutInflater.from(context).inflate(R.layout.dialog_loading_bar, null)
                    val mBuilder = AlertDialog.Builder(context)
                        .setView(mDialogView)
                    val lDialog = mBuilder.show()
                    doAsync {
                        val post = checkContent(mDialog)
                        if (post != null) {
                            if (UserHelper.needsLogin()) {
                                mDialog.dismiss()
                                logout()
                            }
                            if (UserHelper.needNewToken()) {
                                UserHelper.RefreshToken()
                            }
                            val call =
                                PostRepo.service.addPost(UserHelper.ActiveUser?.token!!, post)
                            val postaddCallback = object : Callback<PostRepo.Model.Post> {
                                override fun onFailure(
                                    call: Call<PostRepo.Model.Post>,
                                    t: Throwable
                                ) {

                                }

                                override fun onResponse(
                                    call: Call<PostRepo.Model.Post>,
                                    response: Response<PostRepo.Model.Post>
                                ) {
                                    if (response.isSuccessful) {
                                        getPosts()
                                    }
                                }
                            }
                            call.enqueue(postaddCallback)
                            uiThread {
                                lDialog.dismiss()
                                mDialog.dismiss()
                            }
                        }
                    }

                }
                mDialog.btnSelectPic.setOnClickListener {
                    if (PermissionsHandler.checkAndRequestPermissions(context!!, activity!!)) {
                        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

                        val file =
                            File.createTempFile(
                                "temp_image",
                                ".jpg",
                                context!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                            )
                        val photoURI = FileProvider.getUriForFile(
                            context!!,
                            "korbe.watchdogs.pocketface.fileprovider",
                            file
                        )
                        mCurrentPhotoPath = file.absolutePath

                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        val activityinfo =
                            intent.resolveActivityInfo(context!!.packageManager, intent.flags)
                        if (activityinfo.exported) {
                            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
                        }
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_IMAGE_CAPTURE -> {
                var bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath)
                val ei = ExifInterface(mCurrentPhotoPath)
                val orientation = ei.getAttribute(ExifInterface.TAG_ORIENTATION)
                when (orientation) {
                    ExifInterface.ORIENTATION_ROTATE_90.toString() -> {
                        bitmap = bitmap.rotate(90f)
                    }
                    ExifInterface.ORIENTATION_ROTATE_180.toString() -> {
                        bitmap = bitmap.rotate(180f)
                    }
                    ExifInterface.ORIENTATION_ROTATE_270.toString() -> {
                        bitmap = bitmap.rotate(270f)
                    }
                }
                mDialog.imgContentImg.setImageBitmap(bitmap)
            }
        }
    }

    private fun Bitmap.rotate(degrees: Float): Bitmap {
        val matrix = Matrix().apply { postRotate(degrees) }
        return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
    }

    private fun checkContent(mDialog: AlertDialog?): PostRepo.Model.Post? {
        val post: PostRepo.Model.Post?
        val inputcheck =
            InputChecker().CheckInput(mDialog?.edtContentText?.text.toString(), "Content")
        val img = mDialog!!.imgContentImg.drawable
        if (inputcheck.isNullOrBlank() || img != null) {
            post = PostRepo.Model.Post(
                null,
                UserHelper.ActiveUser,
                null,
                null,
                Calendar.getInstance().time
            )
            if (inputcheck.isNullOrBlank()) {
                post.text = mDialog?.edtContentText?.text.toString()
            }
            if (img != null) {
                post.img = ImageHelper.encodeImage(img)
            }
            return post
        }
        if (!inputcheck.isNullOrBlank()) {
            mDialog?.edtContentText?.error = inputcheck
        }
        return null
    }

    private fun logout() {
        val intent = Intent(context, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    private fun getPosts() {
        isloading = true
        refresher.isRefreshing = true
        if (UserHelper.needsLogin()) {
            logout()
        }
        if (UserHelper.needNewToken()) {
            val refreshtoken = UserHelper.ActiveUser?.refreshtoken
            val call = UserApi.service.refreshToken(refreshtoken!!, UserHelper.ActiveUser!!)
            call.enqueue(tokenCallback)
        } else {
            val token = UserHelper.ActiveUser?.token
            if (askedpage == 0) {
                postlist.clear()
            }
            val call = PostRepo.service.getPosts(token!!, askedpage)
            call.enqueue(postCallback)
        }
    }
}
