package krbe.watchdogs.pocketface

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.dialog_add_image.*
import kotlinx.android.synthetic.main.dialog_add_image.view.*
import kotlinx.android.synthetic.main.dialog_create_post.btnCancel
import kotlinx.android.synthetic.main.dialog_loading_bar.*
import krbe.watchdogs.pocketface.Adapters.GridItemDecoration
import krbe.watchdogs.pocketface.Adapters.ImageAdapter
import krbe.watchdogs.pocketface.Helpers.*
import krbe.watchdogs.pocketface.Repositories.AIRepo
import krbe.watchdogs.pocketface.Repositories.UserApi
import org.opencv.android.OpenCVLoader
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

lateinit var mDialog: AlertDialog
lateinit var mLoadingDialog: AlertDialog
lateinit var mErrorDialog: AlertDialog

class RegisterActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var mCurrentPhotoPath: String
    val imgList = mutableListOf<Bitmap>()
    val aICallback = object : Callback<Void> {
        override fun onFailure(call: Call<Void>, t: Throwable) {
            t.printStackTrace()
        }

        override fun onResponse(call: Call<Void>, response: Response<Void>) {
            if (response.isSuccessful) {
                mLoadingDialog.dismiss()
                val intent = Intent(applicationContext, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
        }

    }
    val REQUEST_IMAGE_CAPTURE = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        btnRegister.setOnClickListener(this)
        OpenCVLoader.initDebug()
        CreateErrorDialog()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_IMAGE_CAPTURE -> {
                var bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath)
                if (bitmap != null) {
                    val ei = ExifInterface(mCurrentPhotoPath)
                    val orientation = ei.getAttribute(ExifInterface.TAG_ORIENTATION)
                    when (orientation) {
                        ExifInterface.ORIENTATION_ROTATE_90.toString() -> {
                            bitmap = bitmap.rotate(90f)
                        }
                        ExifInterface.ORIENTATION_ROTATE_180.toString() -> {
                            bitmap = bitmap.rotate(180f)
                        }
                        ExifInterface.ORIENTATION_ROTATE_270.toString() -> {
                            bitmap = bitmap.rotate(270f)
                        }
                    }
                    bitmap = scaleDown(bitmap, 480f, false)
                    GetFace(bitmap)
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onClick(v: View?) {
        when (v) {
            btnRegister -> {
                if (checkInputs()) {
                    val model = UserApi.Model.User(
                        null,
                        edtUsername.text.toString(),
                        edtEmail.text.toString(),
                        null, null, null, null,
                        getPasswordHash(edtPassword.text.toString()), edtFirstname.text.toString(),
                        edtLastname.text.toString()
                    )
                    createDialog(model)
                }
            }
        }
    }

    private fun scaleDown(
        realImage: Bitmap, maxImageSize: Float,
        filter: Boolean
    ): Bitmap {
        val ratio = Math.min(
            maxImageSize / realImage.width,
            maxImageSize / realImage.height
        )
        val width = Math.round(ratio * realImage.width)
        val height = Math.round(ratio * realImage.height)

        return Bitmap.createScaledBitmap(
            realImage, width,
            height, filter
        )
    }

    private fun GetFace(bitmap: Bitmap) {
        val loadingdialog = LayoutInflater.from(this).inflate(R.layout.dialog_loading_bar, null)
        val builder = AlertDialog.Builder(this)
            .setView(loadingdialog)
            .setCancelable(false)
        val dialog = builder.show()
        dialog.txt_reason.text = "Processing image"
        val detection = FaceDetection(bitmap = bitmap)
        val image = FirebaseVisionImage.fromBitmap(bitmap)
        val options = FirebaseVisionFaceDetectorOptions.Builder()
            .setClassificationMode(FirebaseVisionFaceDetectorOptions.FAST)
            .build()
        val detector = FirebaseVision.getInstance().getVisionFaceDetector(options)
        detector.detectInImage(image).addOnSuccessListener {
            if (!it.isEmpty()) {
                val newbitmap = detection.Crop(bitmap, it)
                imgList.add(newbitmap)
                (mDialog.rcvImages.adapter as ImageAdapter).notifyDataSetChanged()
            } else {
                mErrorDialog.show()
                mErrorDialog.setTitle(getString(R.string.noface_processing))
                mErrorDialog.setMessage(getString(R.string.noface_content_processing))
            }
            dialog.dismiss()
        }
    }

    private fun Bitmap.rotate(degrees: Float): Bitmap {
        val matrix = Matrix().apply { postRotate(degrees) }
        return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
    }

    private fun register(user: UserApi.Model.User, context: Context) {
        val call = UserApi.service.register(user)
        val callback = object : Callback<UserApi.Model.User> {
            override fun onFailure(call: Call<UserApi.Model.User>, t: Throwable) {
                Log.d("WD", t.toString())
                //add Message box
            }

            override fun onResponse(
                call: Call<UserApi.Model.User>,
                response: Response<UserApi.Model.User>
            ) {
                val res: UserApi.Model.User = response.body()!!
                UserHelper.ActiveUser = res
                uploadImages()
            }
        }
        val loadingdialog = LayoutInflater.from(this).inflate(R.layout.dialog_loading_bar, null)
        val builder = AlertDialog.Builder(this)
            .setView(loadingdialog)
            .setCancelable(false)
        mLoadingDialog = builder.show()
        mLoadingDialog.txt_reason.text = getString(R.string.register_loading)
        call.enqueue(callback)
    }

    private fun uploadImages() {
        val sendimgList = mutableListOf<AIRepo.Model.Image>()
        for (img in imgList) {
            sendimgList.add(AIRepo.Model.Image(null, ImageHelper.encodeImage(img), null))
        }
        val call = AIRepo.service.addRecognition(UserHelper.ActiveUser?.token!!, sendimgList)
        call.enqueue(aICallback)

    }

    private fun createDialog(model: UserApi.Model.User) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.dialog_add_image, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
            .setTitle(getString(R.string.add_selfies))
        mDialog = mBuilder.show()
        mDialog.rcvImages.layoutManager = GridLayoutManager(this, 2)
        mDialogView.rcvImages.addItemDecoration(GridItemDecoration(10, 2))
        mDialog.rcvImages.adapter = ImageAdapter(imgList, this, null)

        mDialog.btnCancel.setOnClickListener {
            mDialog.dismiss()
        }
        mDialog.btnPostImages.setOnClickListener {
            if (imgList.size >= 6) {

                register(model, this)
            } else {
                mErrorDialog.show()
                mErrorDialog.setTitle(getString(R.string.not_enough_images_title))
                mErrorDialog.setMessage(getString(R.string.not_enough_images_content))
            }
        }
        mDialog.btnAddImage.setOnClickListener {
            if (PermissionsHandler.checkAndRequestPermissions(this, this)) {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

                val file =
                    File.createTempFile(
                        "temp_image",
                        ".jpg",
                        getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                    )
                val photoURI = FileProvider.getUriForFile(
                    this,
                    "korbe.watchdogs.pocketface.fileprovider",
                    file
                )
                mCurrentPhotoPath = file.absolutePath

                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                val activityinfo =
                    intent.resolveActivityInfo(this.packageManager, intent.flags)
                if (activityinfo.exported) {

                    startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)

                }
            }
        }
    }

    private fun getPasswordHash(password: String): String {
        val hasher = HashingHelper()
        val pwd = hasher.hashWithAlgorithm(password, "SHA-512")
        return pwd
    }

    private fun checkInputs(): Boolean {
        val iChecker = InputChecker()
        val emailError = iChecker.CheckEmail(edtEmail.text.toString())
        val firstNameError = iChecker.CheckInput(edtFirstname.text.toString(), "first name")
        val lastNameError = iChecker.CheckInput(edtLastname.text.toString(), "last name")
        val usernameError = iChecker.CheckInput(edtLastname.text.toString(), "user name")
        val passwordError = iChecker.CheckPassword(edtPassword.text.toString())
        val passwordsError =
            iChecker.CheckPasswords(edtPassword.text.toString(), edtPasswordCheck.text.toString())
        tilUsername.error = usernameError
        tilPasswordCheck.error = passwordsError
        tilPassword.error = passwordError
        tilEmail.error = emailError
        tilLastName.error = lastNameError
        tilFirstName.error = firstNameError
        if (usernameError.isNotEmpty() || emailError.isNotEmpty() || firstNameError.isNotEmpty() ||
            lastNameError.isNotEmpty() || passwordError.isNotEmpty() || passwordsError.isNotEmpty()
        ) {
            return false
        }
        return true
    }

    private fun CreateErrorDialog() {
        val builder = AlertDialog.Builder(this)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton(getString(R.string.ok)) { dialog, which ->
            dialog.dismiss()
        }
        // Finally, make the alert dialog using builder
        mErrorDialog = builder.create()
    }

}
