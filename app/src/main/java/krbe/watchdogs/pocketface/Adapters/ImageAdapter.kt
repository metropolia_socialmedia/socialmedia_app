package krbe.watchdogs.pocketface.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_post.view.*
import krbe.watchdogs.pocketface.R
import krbe.watchdogs.pocketface.Repositories.PostRepo
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.text.SimpleDateFormat


class PostAdapter(
    var list: MutableList<PostRepo.Model.Post> = mutableListOf(),
    val context: Context,
    val onItemClickListener: View.OnClickListener?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PostHolder(
            LayoutInflater.from(context).inflate(R.layout.list_item_post, parent, false)
        )
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun alterlist(altlist: List<PostRepo.Model.Post>) {
        list.clear()
        notifyDataSetChanged()
        list.addAll(altlist)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val view = holder.itemView
        view.txt_postuser.text =
            context.getString(R.string.post_title, list[position].owner?.username)
        val date = SimpleDateFormat("dd-MM-YYYY").format(list[position].postedat!!)
        val time = SimpleDateFormat("HH:mm").format(list[position].postedat!!)
        view.txt_date.text = context.getString(R.string.timeword, date, time)
        view.txt_content.text = list[position].text
        if (!list[position].img.isNullOrBlank()) {
            doAsync {
                val bitmap = Picasso.with(context).load(list[position].img).get()
                uiThread {
                    view.img_content.maxHeight = 350
                    view.img_content.setImageBitmap(bitmap)
                }
            }
        }
        view.setOnClickListener(onItemClickListener)
    }
}

class PostHolder(view: View) : RecyclerView.ViewHolder(view)