package krbe.watchdogs.pocketface.Adapters

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item_image.view.*
import krbe.watchdogs.pocketface.R


class ImageAdapter(
    var list: MutableList<Bitmap> = mutableListOf(),
    val context: Context,
    val onItemClickListener: View.OnClickListener?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PostHolder(
            LayoutInflater.from(context).inflate(R.layout.list_item_image, parent, false)
        )
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val view = holder.itemView
        view.imgImage.maxHeight = 500
        view.imgImage.setImageBitmap(list[position])
        view.setOnClickListener(onItemClickListener)
    }
}