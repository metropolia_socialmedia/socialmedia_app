package krbe.watchdogs.pocketface.Adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_profile.view.*
import kotlinx.android.synthetic.main.fragment_profile.view.txtUsername
import kotlinx.android.synthetic.main.list_item_friends.view.*
import kotlinx.android.synthetic.main.list_item_publicdata.view.*
import krbe.watchdogs.pocketface.Helpers.UserHelper
import krbe.watchdogs.pocketface.R
import krbe.watchdogs.pocketface.Repositories.FriendRepo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileFriendAdapter(
    var list: MutableList<FriendRepo.Model.Friend> = mutableListOf(),
    val context: Context,
    val onItemClickListener: View.OnClickListener?,
    val title: TextView?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PostHolder(
            LayoutInflater.from(context).inflate(R.layout.list_item_friends, parent, false)
        )
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val view = holder.itemView
        view.txtUsername.text = list[position].user?.username
        view.btnRemoveFriend.setOnClickListener {
            view.btnRemoveFriend.visibility = View.INVISIBLE
            view.pgbFriends.visibility = View.VISIBLE
            val call = FriendRepo.service.deleteFriend(
                UserHelper.ActiveUser?.token!!,
                list[position].user?.userid.toString()
            )
            val callback = object : Callback<Void> {
                override fun onFailure(call: Call<Void>, t: Throwable) {
                    view.btnRemoveFriend.visibility = View.VISIBLE
                    view.pgbFriends.visibility = View.GONE
                }

                override fun onResponse(call: Call<Void>, response: Response<Void>) {
                    view.btnRemoveFriend.visibility = View.VISIBLE
                    view.pgbFriends.visibility = View.GONE
                    if (response.isSuccessful) {
                        list.remove(list[position])
                        if (title != null) {
                            title.text = context.getString(R.string.friend_profile_title,list.count())
                        }
                    }
                }
            }
            call.enqueue(callback)
        }
        view.setOnClickListener(onItemClickListener)
    }
}