package krbe.watchdogs.pocketface.Adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item_publicdata.view.*
import krbe.watchdogs.pocketface.R

class ProfileInfoAdapter(
    var list: MutableList<List<String>> = mutableListOf(),
    val context: Context,
    val onItemClickListener: View.OnClickListener?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PostHolder(
            LayoutInflater.from(context).inflate(R.layout.list_item_publicdata, parent, false)
        )
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val view = holder.itemView
        view.txtTitle.text = list[position][0]
        view.txtValue.text = list[position][1]
        if (list[position][2] == "false") {
            view.txtValue.setTextColor(Color.argb(0.5f, 0f, 0f, 0f))
        }
        view.setOnClickListener(onItemClickListener)
    }
}