package krbe.watchdogs.pocketface.CustomComponents

import android.graphics.drawable.Drawable
import android.os.Build
import android.annotation.TargetApi
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.drawable.BitmapDrawable
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import krbe.watchdogs.pocketface.R
import org.jetbrains.anko.attr


class RoundedImageView : ImageView {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    )

    override fun onDrawForeground(canvas: Canvas?) {
        super.onDrawForeground(canvas)
        if (alpha == 2f) {
            var paint = Paint()
            paint.color = context.resources.getColor(R.color.black_alpha)
            paint.style = Paint.Style.FILL
            var radius = width / 2f

            val oval = RectF()
            oval.set(
                width / 2 - radius,
                height / 2 - radius,
                width / 2 + radius,
                height / 2 + radius
            )
            canvas?.drawArc(oval, 30f, 120f, false, paint)
        }
    }

    override fun setImageDrawable(drawable: Drawable?) {
        val radius = 100f
        val bitmap = (drawable as BitmapDrawable).bitmap
        val rid = RoundedBitmapDrawableFactory.create(getResources(), bitmap)
        rid.cornerRadius = bitmap.width * radius
        super.setImageDrawable(rid)
    }

}