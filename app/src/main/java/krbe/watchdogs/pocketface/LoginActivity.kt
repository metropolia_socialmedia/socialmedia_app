package krbe.watchdogs.pocketface

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.PreferenceManager
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.btnRegister
import krbe.watchdogs.pocketface.Helpers.HashingHelper
import krbe.watchdogs.pocketface.Helpers.InputChecker
import krbe.watchdogs.pocketface.Helpers.UserHelper
import krbe.watchdogs.pocketface.Repositories.UserApi
import krbe.watchdogs.pocketface.consts.THEME_PREFERENCE
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {
    private val login = View.OnClickListener {
        { TODO("Add Loading Screen") }
        if (checkInputs()) {
            val context = this
            val hashingHelper = HashingHelper()
            val user = UserApi.Model.User(
                null,
                edtUsername.text.toString(),
                null,
                null,
                null,
                null,
                null,
                hashingHelper.hashWithAlgorithm(edtPassword.text.toString(), "SHA-512"),
                null,
                null
            )

            val call = UserApi.service.login(user)
            val callback = object : Callback<UserApi.Model.User> {
                override fun onFailure(call: Call<UserApi.Model.User>, t: Throwable) {
                    Log.d("WD", t.toString())
                    //add Message box
                }

                override fun onResponse(
                    call: Call<UserApi.Model.User>,
                    response: Response<UserApi.Model.User>
                ) {
                    if (response.isSuccessful) {
                        val res: UserApi.Model.User = response.body()!!
                        UserHelper.ActiveUser = res
                        val intent = Intent(context, MainActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)
                    } else {
                        if (response.code() == 400) {
                            tilUser.error = "Username or Password not Right!"
                            { kotlin.TODO("Change error to Messagebox") }
                        }
                    }
                }
            }
            call.enqueue(callback)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        checkTheme()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btnLogin.setOnClickListener(login)
        btnRegister.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }

    private fun checkInputs(): Boolean {
        val inputchecker = InputChecker()
        val usernameError = inputchecker.CheckInput(edtUsername.text.toString(), "username")
        val passwordError = inputchecker.CheckInput(edtPassword.text.toString(), "password")
        tilUser.error = usernameError
        tilPassword.error = passwordError
        if (usernameError.isNullOrBlank() && passwordError.isNullOrBlank()) {
            return true
        }
        return false
    }

    private fun checkTheme() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        when (prefs.getString(THEME_PREFERENCE, "AUTO")) {
            "LIGHT" -> AppCompatDelegate.setDefaultNightMode(
                AppCompatDelegate.MODE_NIGHT_NO
            )
            "DARK" -> AppCompatDelegate.setDefaultNightMode(
                AppCompatDelegate.MODE_NIGHT_YES
            )
            "AUTO" -> {
                AppCompatDelegate.setDefaultNightMode(
                    AppCompatDelegate.MODE_NIGHT_AUTO_TIME
                )
            }
        }
    }
}
