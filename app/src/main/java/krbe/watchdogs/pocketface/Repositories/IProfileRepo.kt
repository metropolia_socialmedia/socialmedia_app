package krbe.watchdogs.pocketface.Repositories

import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

object ProfileRepo {

    object Model {
        data class Profile(
            val values: List<List<String>>?,
            val succeedList: List<List<Boolean>>?
        )
    }

    interface IProfileRepo {
        @GET("profile")
        fun getUserProfile(@Header("Authorization") Authorization: String, @Query("profileId") profileId: String): Call<Model.Profile>

        @POST("profile")
        fun addToProfile(@Header("Authorization") Authorization: String, @Body action: Model.Profile): Call<Model.Profile>

        @PUT("profile")
        fun EditProfile(@Header("Authorization") Authorization: String, @Body action: Model.Profile): Call<Model.Profile>

        @DELETE("profile")
        fun deleteFromProfile(@Header("Authorization") Authorization: String, @Query("ProfileInfoID") ProfileInfoID: String): Call<Any>
    }

    val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()
    private val retrofit = Retrofit.Builder()
        .baseUrl(UserApi.URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
    val service = retrofit.create(IProfileRepo::class.java)
}