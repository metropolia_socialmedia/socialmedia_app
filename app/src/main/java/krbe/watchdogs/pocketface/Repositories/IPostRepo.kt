package krbe.watchdogs.pocketface.Repositories

import com.google.gson.GsonBuilder
import krbe.watchdogs.pocketface.Repositories.UserApi.URL
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.*

object PostRepo {

    object Model {
        data class Post(
            val postid: String?,
            var owner: UserApi.Model.User?,
            var text: String?,
            var img: String?,
            var postedat: Date?
        )
    }

    interface IPostRepo {
        @GET("post/user")
        fun getUserPosts(@Header("Authorization") Authorization: String, @Query("page") page: Int): Call<List<Model.Post>>

        @GET("post")
        fun getPosts(@Header("Authorization") Authorization: String, @Query("page") page: Int): Call<List<Model.Post>>

        @POST("post")
        fun addPost(@Header("Authorization") Authorization: String, @Body action: Model.Post): Call<Model.Post>
    }

    val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()
    private val retrofit = Retrofit.Builder()
        .baseUrl(URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
    val service = retrofit.create(IPostRepo::class.java)
}