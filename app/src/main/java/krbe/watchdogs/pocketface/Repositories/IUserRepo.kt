package krbe.watchdogs.pocketface.Repositories

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import java.util.*

object UserApi {
    const val URL = "http://192.168.137.1:59066/api/"

    object Model {
        data class User(
            val userid: String?,
            var username: String?,
            var email: String?,
            var token: String?,
            var tokenEndTime: Date?,
            var refreshtoken: String?,
            var refreshtokenEndTime: Date?,
            var password: String?,
            var firstname: String?,
            var lastname: String?
        )
    }

    interface IUserRepo {
        @POST("user/Register")
        fun register(@Body action: Model.User): Call<Model.User>

        @POST("user/Login")
        fun login(@Body action: Model.User): Call<Model.User>

        @POST("user/Token")
        fun refreshToken(@Header("Authorization") Authorization: String, @Body action: Model.User): Call<Model.User>
    }

    private val retrofit = Retrofit.Builder()
        .baseUrl(URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    val service = retrofit.create(IUserRepo::class.java)
}