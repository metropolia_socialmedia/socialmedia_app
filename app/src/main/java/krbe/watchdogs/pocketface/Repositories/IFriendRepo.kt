package krbe.watchdogs.pocketface.Repositories

import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

object FriendRepo {

    object Model {
        data class Friend(
            val id: Int?,
            val user: UserApi.Model.User?,
            val profile: ProfileRepo.Model.Profile?
        )
    }

    interface IFriendRepo {
        @POST("friend")
        fun addFriend(@Header("Authorization") Authorization: String, @Body action: UserApi.Model.User): Call<Model.Friend>

        @DELETE("friend")
        fun deleteFriend(@Header("Authorization") Authorization: String, @Query("friendid") friendid: String): Call<Void>

        @GET("friend")
        fun getFriends(@Header("Authorization") Authorization: String): Call<List<Model.Friend>>
    }

    val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()
    private val retrofit = Retrofit.Builder()
        .baseUrl(UserApi.URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
    val service = retrofit.create(IFriendRepo::class.java)
}