package krbe.watchdogs.pocketface.Repositories

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

object AIRepo {

    object Model {
        data class Image(
            val imageid: String?,
            val base64: String?,
            val imageUrl: String?
        )
    }

    interface IAIRepo {
        @POST("recognition/add")
        fun addRecognition(@Header("Authorization") Authorization: String, @Body action: List<Model.Image>): Call<Void>

        @POST("recognition")
        fun recognize(@Header("Authorization") Authorization: String, @Body action: Model.Image): Call<FriendRepo.Model.Friend>
    }

    val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()

    private val retrofit = Retrofit.Builder()
        .baseUrl(UserApi.URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(
            OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS).readTimeout(
                120,
                TimeUnit.SECONDS
            ).build()
        )
        .build()
    val service = retrofit.create(IAIRepo::class.java)
}