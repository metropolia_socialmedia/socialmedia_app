package krbe.watchdogs.pocketface.Repositories

import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

object ImageRepo {
    interface IImageRepo {
        @GET("image")
        fun getImages(@Header("Authorization") Authorization: String): Call<List<AIRepo.Model.Image>>
    }

    val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()
    private val retrofit = Retrofit.Builder()
        .baseUrl(UserApi.URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
    val service = retrofit.create(IImageRepo::class.java)
}