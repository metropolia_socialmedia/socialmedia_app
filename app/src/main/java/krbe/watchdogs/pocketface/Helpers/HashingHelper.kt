package krbe.watchdogs.pocketface.Helpers

import java.security.MessageDigest

class HashingHelper {

    fun hashWithAlgorithm(password: String, algorithm: String): String {
        val digest = MessageDigest.getInstance(algorithm)
        val bytes = digest.digest(password.toByteArray(Charsets.UTF_8))
        return bytes.fold("", { str, it -> str + "%02x".format(it) })
    }
}