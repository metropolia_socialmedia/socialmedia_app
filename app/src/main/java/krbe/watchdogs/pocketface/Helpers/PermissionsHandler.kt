package krbe.watchdogs.pocketface.Helpers

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class PermissionsHandler {
    companion object {
        val PERMISSION_REQUEST_CODE = 1240
        val appPermissions: Array<String> = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )

        fun checkAndRequestPermissions(context: Context, activity: Activity): Boolean {
            val listPermissionsNeeded = arrayListOf<String>()
            for (perm in appPermissions) {
                if (ContextCompat.checkSelfPermission(
                        context,
                        perm
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    listPermissionsNeeded.add(perm)
                }
            }
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(
                    activity,
                    listPermissionsNeeded.toArray(arrayOf()),
                    PERMISSION_REQUEST_CODE
                )
                return false
            }
            return true
        }
    }
}