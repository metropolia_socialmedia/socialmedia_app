package krbe.watchdogs.pocketface.Helpers

import android.text.TextUtils

class InputChecker {
    fun CheckEmail(email: String?): String {
        val check = CheckInput(email, "email")
        if (check != "") {
            return check
        }
        if (!email!!.isEmailValid()) {
            return "This is not a valid email!"
        }
        return ""
    }

    private fun String.isEmailValid(): Boolean {
        return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }

    fun CheckInput(input: String?, type: String): String {
        if (input.isNullOrBlank()) {
            return "Fill in your: $type"
        }
        return ""
    }

    fun CheckPassword(password: String): String {
        val check = CheckInput(password, "password")
        if (check != "") {
            return check
        }
        if (password.length < 6) {
            return "Password must be at least 6 characters"
        }
        return ""
    }

    fun CheckPasswords(password: String, passwordCheck: String): String {
        val check = CheckPassword(passwordCheck)
        if (check != "") {
            return check
        }
        if (password != passwordCheck) {
            return "Passwords are not the same"
        }
        return ""
    }

}