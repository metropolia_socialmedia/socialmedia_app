package krbe.watchdogs.pocketface.Helpers

import android.graphics.Bitmap
import android.graphics.Bitmap.createBitmap
import android.graphics.Matrix
import android.media.Image
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import org.opencv.core.Mat
import com.quickbirdstudios.yuv2mat.Yuv
import org.opencv.android.Utils
import org.opencv.core.Point
import org.opencv.core.Rect
import org.opencv.core.Scalar
import org.opencv.imgproc.Imgproc


class FaceDetection(val image: Image? = null, var bitmap: Bitmap? = null) {
    fun getBitmapobject(): Bitmap {
        //gives a false error
        val mat: Mat = Yuv.rgb(image!!)
        val bmp = createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888)
        Utils.matToBitmap(mat, bmp)
        bitmap = bmp.rotate(90f)
        return bmp.rotate(90f)
    }


    fun Bitmap.rotate(degrees: Float): Bitmap {
        val matrix = Matrix().apply { postRotate(degrees) }
        return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
    }

    fun drawRect(bitmap: Bitmap, faces: List<FirebaseVisionFace>): Bitmap {
        val mat = Mat()
        Utils.bitmapToMat(bitmap, mat)
        val bounds = faces.first().boundingBox
        Imgproc.rectangle(
            mat,
            Point((bounds.left - 50).toDouble(), (bounds.top - 50).toDouble()),
            Point((bounds.right + 50).toDouble(), (bounds.bottom + 50).toDouble()),
            Scalar(0.0, 255.0, 0.0, 255.0)
        )
        val bmp =
            createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888)
        Utils.matToBitmap(mat, bmp)
        return bmp
    }

    fun Crop(bitmap: Bitmap, faces: List<FirebaseVisionFace>): Bitmap {
        val bounds = faces.first().boundingBox
        val mat = Mat()
        Utils.bitmapToMat(bitmap, mat)
        val roi = Rect(
            Point(bounds.left.toDouble(), bounds.top.toDouble()),
            Point(bounds.right.toDouble(), bounds.bottom.toDouble())
        )
        val croppedmat = Mat(mat, roi)
        val newbitmap = createBitmap(croppedmat.cols(), croppedmat.rows(), Bitmap.Config.ARGB_8888)
        Utils.matToBitmap(croppedmat, newbitmap)
        return newbitmap
    }
}