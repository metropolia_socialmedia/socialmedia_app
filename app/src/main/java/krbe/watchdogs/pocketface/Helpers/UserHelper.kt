package krbe.watchdogs.pocketface.Helpers

import krbe.watchdogs.pocketface.Repositories.UserApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class UserHelper {
    companion object {
        var ActiveUser: UserApi.Model.User? = null
        fun needNewToken(): Boolean {
            if (ActiveUser?.tokenEndTime!! < Calendar.getInstance().time) {
                return true
            }
            return false
        }

        fun needsLogin(): Boolean {
            if (ActiveUser?.refreshtokenEndTime!! < Calendar.getInstance().time) {
                return true
            }
            return false
        }

        fun RefreshToken() {
            val tokenCallback = object : Callback<UserApi.Model.User> {
                override fun onFailure(call: Call<UserApi.Model.User>, t: Throwable) {
                }

                override fun onResponse(
                    call: Call<UserApi.Model.User>,
                    response: Response<UserApi.Model.User>
                ) {
                    if (response.isSuccessful) {
                        UserHelper.ActiveUser = response.body()
                    }
                }
            }
            val call = UserApi.service.refreshToken(ActiveUser?.token!!, UserHelper.ActiveUser!!)
            call.enqueue(tokenCallback)
        }
    }
}