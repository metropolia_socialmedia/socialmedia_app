package krbe.watchdogs.pocketface.Helpers


import android.graphics.drawable.Drawable
import androidx.core.graphics.drawable.toBitmap
import java.util.*
import java.io.File
import android.graphics.Bitmap
import android.R.attr.bitmap
import java.io.ByteArrayOutputStream


class ImageHelper {
    companion object {
        fun encodeImage(bitmap: Drawable): String {
            val b = bitmap.toBitmap()
            val byteArrayOutputStream = ByteArrayOutputStream()
            b.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
            val byteArray = byteArrayOutputStream.toByteArray()
            return Base64.getEncoder().encodeToString(byteArray)
        }
        fun encodeImage(bitmap: Bitmap): String {
            val byteArrayOutputStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
            val byteArray = byteArrayOutputStream.toByteArray()
            return Base64.getEncoder().encodeToString(byteArray)
        }
    }
}